const addToCartAnimation = (el) => {
  el.style.display = 'block'

  const position = el.getBoundingClientRect()

  el.style.top = `-${parseInt(position.top, 10)}px`
  el.style.left = `${parseInt(document.body.clientWidth - position.left + position.width, 10)}px`
  el.style.zIndex = 9999;
  el.style.width = '1px';
  el.style.height = '1px';
  el.style.minWidth = '1px';

  setTimeout(() => {
    el.style.display = 'none'
    el.style.top = '0'
    el.style.left = '100%'
    el.style.zIndex = 0
    el.style.width = '100%';
    el.style.height = '100%';
    el.style.minWidth = '100%';
  }, 200);
}

export default addToCartAnimation
