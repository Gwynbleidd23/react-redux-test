import { connect } from 'react-redux'
import { closeProductDetails, addToCart } from '../actions'
import ProductDetails from '../components/ProductDetails'

const mapStateToProps = (state) => {
  return ({
    open: state.productDetails.open,
    product: state.productDetails.product
  })
}

const mapDispatchToProps = {
  closeProductDetails: closeProductDetails,
  addToCart: addToCart
}

const ProductDetailsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductDetails)

export default ProductDetailsContainer
