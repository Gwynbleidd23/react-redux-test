import { connect } from 'react-redux'
import { setCartState, removeFromCart } from '../actions'
import Cart from '../components/Cart'

const mapStateToProps = (state) => {
  return ({
    open: state.cart.open,
    total: state.cart.total,
    products: state.cartProducts
  })
}

const mapDispatchToProps = {
  setState: setCartState,
  removeFromCart
}

const CartContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart)

export default CartContainer
