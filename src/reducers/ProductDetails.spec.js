import ProductDetails from './ProductDetails'

describe('ProductDetails reducer', () => {
  it('should accept undefined value', () => {
    expect(ProductDetails(undefined, {type: 'SOME_ACTION'})).toEqual({open: false})
  })

  it('should skip not declared actions', () => {
    expect(ProductDetails({open: true}, {type: 'SOME_ACTION'}))
      .toEqual({open: true})
    expect(ProductDetails({open: false}, {type: 'SOME_ACTION'}))
      .toEqual({open: false})
  })

  it('should set state to false on CLOSE_PRODUCT_DETAILS action', () => {
    expect(ProductDetails({open: true}, {type: 'CLOSE_PRODUCT_DETAILS'}))
      .toEqual({
        open: false,
        product: null
      })
    expect(ProductDetails({open: false}, {type: 'CLOSE_PRODUCT_DETAILS'}))
      .toEqual({
        open: false,
        product: null
      })
  })

  it('should set state to false on CLOSE_PRODUCT_DETAILS action', () => {
    expect(ProductDetails({
      open: true,
      product: {
        id: 2,
        name: 'test2',
        description: 'desc2',
        price: 2
      }
    }, {
      type: 'OPEN_PRODUCT_DETAILS',
      product: {
        id: 1,
        name: 'test1',
        description: 'desc1',
        price: 1
      }
    })).toEqual({
      open: true,
      product: {
        id: 1,
        name: 'test1',
        description: 'desc1',
        price: 1
      }
    })

    expect(ProductDetails({open: false}, {
      type: 'OPEN_PRODUCT_DETAILS',
      product: {
        id: 1,
        name: 'test1',
        description: 'desc1',
        price: 1
      }}))
      .toEqual({
        open: true,
        product: {
          id: 1,
          name: 'test1',
          description: 'desc1',
          price: 1
        }
      })
  })
})
