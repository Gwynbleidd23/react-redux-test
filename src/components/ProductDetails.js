import React, {PropTypes} from 'react'
import Drawer from 'material-ui/Drawer'
import {Card, CardMedia, CardTitle, CardText} from 'material-ui/Card'
import ContentAdd from 'material-ui/svg-icons/content/add'
import FloatingActionButton from 'material-ui/FloatingActionButton'

const addButtonStyle = {
  float: 'right',
  minWidth: 'auto',
  width: 'auto',
  paddingTop: 5,
  margin: '0 5px -61px 0px'
}

const ProductDetails = ({product, open, closeProductDetails, addToCart}) => (
  <Drawer
    docked={false}
    width={300}
    open={open}
    onRequestChange={() => closeProductDetails()}
  >
    {product &&
      <Card>
        <CardMedia>
          <div style={addButtonStyle}>
            <FloatingActionButton onClick={(e) => {e.stopPropagation(); addToCart(product)}}>
              <ContentAdd />
            </FloatingActionButton>
          </div>
          <img
            alt={product.description}
            src={product.image}
          />
        </CardMedia>
        <CardTitle title={product.name} subtitle={product.price + ' $'} />
        <CardText>
          {product.description}
        </CardText>
      </Card>
    }
  </Drawer>
)

ProductDetails.propTypes = {
  product: PropTypes.object,
  open: PropTypes.bool.isRequired,
  closeProductDetails: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
}

export default ProductDetails
