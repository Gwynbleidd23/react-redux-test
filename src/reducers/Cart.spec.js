import Cart from './Cart'

describe('cart reducer', () => {
  it('should accept undefined value', () => {
    expect(Cart(undefined, {type: 'SOME_ACTION'})).toEqual({open: false, total: 0})
  })

  it('should skip not declared actions', () => {
    expect(Cart({open: true}, {type: 'SOME_ACTION'})).toEqual({open: true})
    expect(Cart({open: false}, {type: 'SOME_ACTION'})).toEqual({open: false})
  })

  it('should set state to false on HIDE_CART action', () => {
    expect(Cart({open: true}, {type: 'HIDE_CART'})).toEqual({open: false})
    expect(Cart({open: false}, {type: 'HIDE_CART'})).toEqual({open: false})
  })

  it('should set state on SET_CART_STATE', () => {
    expect(Cart({open: true}, {type: 'SET_CART_STATE', open: true}))
      .toEqual({open: true})
    expect(Cart({open: false}, {type: 'SET_CART_STATE', open: true}))
      .toEqual({open: true})
    expect(Cart({open: true}, {type: 'SET_CART_STATE', open: false}))
      .toEqual({open: false})
    expect(Cart({open: false}, {type: 'SET_CART_STATE', open: false}))
      .toEqual({open: false})
  })

  it('should calculate total sum on ADD_TO_CART', () => {
    expect(Cart({open: false, total: 500},
      {type: 'ADD_TO_CART', product: {price: 300}})
    ).toEqual({open: false, total: 800})
  })

  it('should calculate total sum on REMOVE_FROM_CART', () => {
    expect(Cart({open: false, total: 500},
      {type: 'REMOVE_FROM_CART', product: {price: 300}})
    ).toEqual({open: false, total: 200})
  })
})
