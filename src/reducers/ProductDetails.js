const ProductDetails = (state = {open: false}, action) => {
  switch (action.type) {
    case 'CLOSE_PRODUCT_DETAILS':
      return {
        ...state,
        open: false,
        product: null
      }
    case 'OPEN_PRODUCT_DETAILS':
      return {
        ...state,
        open: true,
        product: action.product
      }
    default:
      return state
  }
}

export default ProductDetails
