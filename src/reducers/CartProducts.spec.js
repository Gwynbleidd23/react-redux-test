import CartProducts from './CartProducts'

describe('CartProducts reducer', () => {
  it('should skip not declared actions', () => {
    expect(CartProducts([], {type: 'SOME_ACTION'})).toEqual([])

    expect(CartProducts([{
      id: 1,
      name: 'test1',
      count: 1
    }], {type: 'SOME_ACTION'})).toEqual([{
      id: 1,
      name: 'test1',
      count: 1
    }])
  })

  it('should add new product to empty cart', () => {
    expect(CartProducts([], {
      type: 'ADD_TO_CART',
      product: {
        id: 1,
        name: 'test1'
      }
    })).toEqual([{
      id: 1,
      name: 'test1',
      count: 1
    }])
  })

  it('should add new product to not empty cart', () => {
    expect(CartProducts([{
      id: 2,
      name: 'test2',
      count: 2,
    }], {
      type: 'ADD_TO_CART',
      product: {
        id: 1,
        name: 'test1'
      }
    })).toEqual([{
        id: 2,
        name: 'test2',
        count: 2,
      }, {
        id: 1,
        name: 'test1',
        count: 1
      }
    ])
  })

  it('should increment count for existing product on adding', () => {
    expect(CartProducts([{
      id: 1,
      name: 'test1',
      count: 3,
    }, {
      id: 2,
      name: 'test2',
      count: 2,
    }], {
      type: 'ADD_TO_CART',
      product: {
        id: 1,
        name: 'test1'
      }
    })).toEqual([{
        id: 1,
        name: 'test1',
        count: 4,
      }, {
        id: 2,
        name: 'test2',
        count: 2
      }
    ])
  })

  it('should remove single product from cart on REMOVE_FROM_CART', () => {
    expect(CartProducts([{
      name: 'test1',
      price: 100,
      count: 1,
      id: 1
    }], {
      type: 'REMOVE_FROM_CART',
      product: {
        name: 'test1',
        price: 100,
        id: 1
      }
    })).toEqual([])
  })

  it('should decrement count of product on REMOVE_FROM_CART', () => {
    expect(CartProducts([{
      name: 'test1',
      price: 100,
      count: 2,
      id: 1
    }], {
      type: 'REMOVE_FROM_CART',
      product: {
        name: 'test1',
        price: 100,
        id: 1
      }
    })).toEqual([{
      name: 'test1',
      price: 100,
      count: 1,
      id: 1
    }])
  })

})
