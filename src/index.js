import React from 'react'
import { render } from 'react-dom'
import { applyMiddleware, createStore } from 'redux'
import { Provider } from 'react-redux'
import App from './components/App'
import Contacts from './components/Contacts'
import VisibleProductList from './containers/VisibleProductList'
import reducer from './reducers'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Route, BrowserRouter } from 'react-router-dom'
import GetProducts from './fakeBackend/Products'

import {updateProducts} from './actions/index'

import logger from 'redux-logger'

const store = createStore(reducer, applyMiddleware(logger))

injectTapEventPlugin();

GetProducts('').then((data) => {
  store.dispatch(updateProducts(data))
}).catch((e) => {
  console.log('some error on GetProducts', e);
})

render(
  <MuiThemeProvider>
    <Provider store={store}>
      <BrowserRouter>
        <App>
          <Route exact={true} path="/" component={VisibleProductList}/>
          <Route path="/contacts" component={Contacts}/>
        </App>
      </BrowserRouter>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('root')
)
