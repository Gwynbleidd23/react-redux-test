import { connect } from 'react-redux'
import { addToCart, openProductDetails } from '../actions'
import ProductList from '../components/ProductList'

const mapStateToProps = (state) => {
  return ({
    products: state.products
  })
}

const mapDispatchToProps = {
  onAddClick: addToCart,
  onProductClick: openProductDetails
}

const VisibleProductList = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductList)

export default VisibleProductList
