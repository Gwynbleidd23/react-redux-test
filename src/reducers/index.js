import { combineReducers } from 'redux'
import CartProducts from './CartProducts'
import Products from './Products'
import Cart from './Cart'
import ProductDetails from './ProductDetails'

const todoApp = combineReducers({
  cartProducts: CartProducts,
  products: Products,
  cart: Cart,
  productDetails: ProductDetails
})

export default todoApp
