import * as actions from './index'

describe('Cart actions', () => {
  it('hideCart should create HIDE_CART action', () => {
    expect(actions.hideCart()).toEqual({
      type: 'HIDE_CART'
    })
  })

  it('setCartState should create SET_CART_STATE action', () => {
    expect(actions.setCartState(true)).toEqual({
      type: 'SET_CART_STATE',
      open: true
    })
  })
})

describe('CartProducts actions', () => {
  it('addToCart should create ADD_TO_CART action', () => {
    expect(actions.addToCart({
      name: 'test1',
      price: 200,
      id: 1})
    ).toEqual({
      type: 'ADD_TO_CART',
      product: {
        name: 'test1',
        price: 200,
        id: 1
      }
    })
  })

  it('removeFromCart should create REMOVE_FROM_CART action', () => {
    expect(actions.removeFromCart({
      name: 'test1',
      price: 200,
      id: 1})
    ).toEqual({
      type: 'REMOVE_FROM_CART',
      product: {
        name: 'test1',
        price: 200,
        id: 1
      }
    })
  })
})

describe('products actions', () => {
  it('updateProducts should create UPDATE_PRODUCTS action', () => {
    expect(actions.updateProducts([{
      name: 'test1',
      description: 'desc1',
      id: 1}])
    ).toEqual({
      type: 'UPDATE_PRODUCTS',
      products: [{
        name: 'test1',
        description: 'desc1',
        id: 1}]
    })
  })
})

describe('ProductDetails actions', () => {
  it('openProductDetails should create OPEN_PRODUCT_DETAILS action', () => {
    expect(actions.openProductDetails({
      id: 1,
      price: 1,
      name: 'test1',
      description: 'desc1'
    })).toEqual({
      type: 'OPEN_PRODUCT_DETAILS',
      product: {
        id: 1,
        price: 1,
        name: 'test1',
        description: 'desc1'
      }
    })
  })

  it('closeProductDetails should create CLOSE_PRODUCT_DETAILS action', () => {
    expect(actions.closeProductDetails()).toEqual({
      type: 'CLOSE_PRODUCT_DETAILS'
    })
  })
})
