import React from 'react'
import {Table, TableBody, TableRow, TableRowColumn} from 'material-ui/Table'

const Contacts = () => (
  <Table>
    <TableBody displayRowCheckbox={false}>
      <TableRow>
        <TableRowColumn>Address</TableRowColumn>
        <TableRowColumn>74 Green Street, Tunapuna, Trinidad W.I.</TableRowColumn>
      </TableRow>
      <TableRow>
        <TableRowColumn>Phone</TableRowColumn>
        <TableRowColumn>+14155552671</TableRowColumn>
      </TableRow>
    </TableBody>
  </Table>
)

export default Contacts
