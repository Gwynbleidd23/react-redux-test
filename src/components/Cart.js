import React, { PropTypes } from 'react'
import Drawer from 'material-ui/Drawer'
import {List, ListItem} from 'material-ui/List'
import Avatar from 'material-ui/Avatar';
import Subheader from 'material-ui/Subheader'
import ContentRemoveCircle from 'material-ui/svg-icons/content/remove-circle';
import {red500} from 'material-ui/styles/colors';

const Cart = ({products, open, total, setState, removeFromCart}) => (
  <Drawer
    docked={false}
    width={300}
    open={open}
    onRequestChange={(open) => setState(open)}
  >
    <List>
      <Subheader inset={true}>Total: {total} $</Subheader>
      {products.map((product) =>
        <ListItem
          key={product.id}
          leftAvatar={<Avatar src={product.image} />}
          rightIcon={<ContentRemoveCircle
            onClick={() => {removeFromCart(product)}}
            color={red500}
          />}
          primaryText={product.name}
          secondaryText={'count: ' + product.count + ' ('+ product.price * product.count + ' $)'}
        />
      )}
    </List>

  </Drawer>
)

Cart.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired
  }).isRequired).isRequired,
  open: PropTypes.bool.isRequired,
  total: PropTypes.number.isRequired,
  setState: PropTypes.func.isRequired,
  removeFromCart: PropTypes.func.isRequired,
}

export default Cart
