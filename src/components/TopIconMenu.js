import React, {PropTypes} from 'react'
import FlatButton from 'material-ui/FlatButton'
import ActionShoppingCart from 'material-ui/svg-icons/action/shopping-cart'
import ActionList from 'material-ui/svg-icons/action/list'
import CommunicationPhone from 'material-ui/svg-icons/communication/phone'
import {white} from 'material-ui/styles/colors'
import {Link} from 'react-router-dom'

const buttonStyle = {
  backgroundColor: 'transparent',
  color: white,
  marginTop: '5px',
  textTransform: 'uppercase'
};

const iconStyle = {
  fill: white,
  height: 24,
  width: 24,
  marginLeft: 12,
  marginRight: 8,
  verticalAlign: 'middle',
}

const TopIconMenu = ({setCartState}) => (
  <div>
    <Link to="/">
      <FlatButton style={buttonStyle} >
        <ActionList style={iconStyle}/>
        <span className="hide-xs">Product list</span>
      </FlatButton>
    </Link>
    <Link to="/contacts">
      <FlatButton style={buttonStyle} >
        <CommunicationPhone style={iconStyle}/>
        <span className="hide-xs">Contacts</span>
      </FlatButton>
    </Link>
    <FlatButton
      style={buttonStyle}
      onClick={() => setCartState(true)}
    >
      <ActionShoppingCart style={iconStyle}/>
      <span className="hide-xs">Cart</span>
    </FlatButton>
  </div>
)

TopIconMenu.propTypes = {
  setCartState: PropTypes.func.isRequired
}

export default TopIconMenu;
