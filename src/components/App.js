import React from 'react'
import AppBar from 'material-ui/AppBar';
import TopIconMenu from '../containers/TopIconMenu'
import Cart from '../containers/Cart'
import '../styles/index.css'

const appBarStyle = {
  position: 'fixed',
  top: 0
}

const containerStyle = {
  marginTop: 64,
}

const App = ({children}) => (
  <div style={containerStyle}>
    <AppBar
      style={appBarStyle}
      iconElementRight={<TopIconMenu></TopIconMenu>}
      showMenuIconButton={false}
    />
    <Cart/>
    {children}
  </div>
)

export default App
