const Cart = (state = {open: false, total: 0}, action) => {
  switch (action.type) {
    case 'HIDE_CART':
      return {
        ...state,
        open: false
      }
    case 'SET_CART_STATE':
      return {
        ...state,
        open: action.open
      }
    case 'ADD_TO_CART':
      return {
        ...state,
        total: state.total + action.product.price
      }
    case 'REMOVE_FROM_CART':
      return {
        ...state,
        total: state.total - action.product.price
      }
    default:
      return state
  }
}

export default Cart
