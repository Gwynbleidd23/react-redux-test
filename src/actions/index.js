export const addToCart = (product) => ({
  type: 'ADD_TO_CART',
  product: product
})

export const removeFromCart = (product) => ({
  type: 'REMOVE_FROM_CART',
  product: product
})

export const updateProducts = (products) => ({
  type: 'UPDATE_PRODUCTS',
  products: products
})

export const setCartState = (open) => ({
  type: 'SET_CART_STATE',
  open
})

export const openProductDetails = (product) => ({
  type: 'OPEN_PRODUCT_DETAILS',
  product
})

export const closeProductDetails = () => ({
  type: 'CLOSE_PRODUCT_DETAILS'
})

export const hideCart = () => ({
  type: 'HIDE_CART'
})
