import Products from './Products'

describe('Products reducer', () => {
  it('should skip not declared actions', () => {
    expect(Products([], {type: 'SOME_ACTION'})).toEqual([])

    expect(Products([{
      id: 1,
      name: 'test1',
      description: 'desc1'
    }], {type: 'SOME_ACTION'})).toEqual([{
      id: 1,
      name: 'test1',
      description: 'desc1'
    }])
  })

  it('should update empty product list', () => {
    expect(Products([], {
      type: 'UPDATE_PRODUCTS',
      products: [{
        id: 1,
        name: 'test1',
        description: 'desc1'
      }, {
        id: 2,
        name: 'test2',
        description: 'desc2'
      }]
    })).toEqual([{
      id: 1,
      name: 'test1',
      description: 'desc1'
    }, {
      id: 2,
      name: 'test2',
      description: 'desc2'
    }])
  })

  it('should update not empty product list', () => {
    expect(Products([{
      id: 1,
      name: 'test1',
      description: 'desc1'
    }], {
      type: 'UPDATE_PRODUCTS',
      products: [{
        id: 2,
        name: 'test2',
        description: 'desc2'
      }, {
        id: 3,
        name: 'test3',
        description: 'desc3'
      }]
    })).toEqual([{
      id: 2,
      name: 'test2',
      description: 'desc2'
    }, {
      id: 3,
      name: 'test3',
      description: 'desc3'
    }])
  })
})
