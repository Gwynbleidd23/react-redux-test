const _products = [
  {name: 'Margherita', description: 'tomato sauce, mozzarella', id: 1, image: '/images/1.jpg', price: 5},
  {name: 'Funghi', description: 'tomato sauce, mozzarella, mushrooms', id: 2, image: '/images/2.jpg', price: 6},
  {name: 'Capricciosa', description: 'tomato sauce, mozzarella, mushrooms, ham, eggs, artichoke, cocktail sausages, green olives', id: 3, image: '/images/3.jpg', price: 7},
  {name: 'Quattro Stagioni', description: 'tomato sauce, mozzarella, ham, black olives, mushrooms, artichoke, peas, salami, eggs', id: 4, image: '/images/4.jpg', price: 8},
  {name: 'Vegetariana', description: 'tomato sauce, mozzarella, mushrooms, onion, (artichoke), sweet corn, green peppers', id: 5, image: '/images/5.jpg', price: 9},
  {name: 'Quattro Formaggi', description: 'tomato sauce, and 4 assorted cheeses, generally mozzarella, Parmesan cheese, blue cheese, and goat cheese, but may vary', id: 6, image: '/images/6.jpg', price: 5},
  {name: 'Marinara', description: 'tomato sauce, mozzarella, shrimps, mussels, tuna, calamari, crab meat', id: 7, image: '/images/1.jpg', price: 6},
  {name: 'Peperoni', description: 'tomato sauce, mozzarella, peperoni', id: 8, image: '/images/2.jpg', price: 7},
  {name: 'Napolitana', description: 'tomato sauce, anchovies, olives, capers', id: 9, image: '/images/3.jpg', price: 8},
  {name: 'Hawaii', description: 'tomato sauce, mozzarella, ham, pineapple', id: 10, image: '/images/4.jpg', price: 9},
  {name: 'Maltija', description: 'tomato sauce, goat cheese, sun dried tomatoes, Maltese sausage, onion', id: 11, image: '/images/5.jpg', price: 5},
  {name: 'Calzone', description: 'tomato sauce, mozzarella, mushrooms, ham, eggs', id: 12, image: '/images/6.jpg', price: 6},
]
const GetProducts = (filter = '') =>
  new Promise(resolve =>
    setTimeout(() => {
      const result = _products.filter((p) => p.name.indexOf(filter) >= 0)
      resolve(result)
    }, 500)
  )


export default GetProducts
