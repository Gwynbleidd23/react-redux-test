const CartProduct = (state, action) => {
  switch (action.type) {
    case 'ADD_TO_CART':
      if (state.id !== action.product.id) {
        return state
      }
      return {
        ...state,
        count: ++state.count
      }
    case 'REMOVE_FROM_CART':
      if (state.id !== action.product.id) {
        return state
      }
      return {
        ...state,
        count: --state.count
      }
    default:
      return state
  }
}

const CartProducts = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TO_CART':
      const prod = state.filter(p => p.id === action.product.id)[0];
      if (prod) {
        return state.map(p =>
          CartProduct(p, action)
        )
      }
      return [
        ...state,
        {
          ...action.product,
          count: 1
        }
      ]
    case 'REMOVE_FROM_CART':
      return state.map(p =>
        CartProduct(p, action)
      ).filter(p =>
        p.count > 0
      )
    default:
      return state
  }
}

export default CartProducts
