import React, { PropTypes } from 'react'
import Paper from 'material-ui/Paper';
import {Card, CardMedia, CardTitle} from 'material-ui/Card'
import ContentAdd from 'material-ui/svg-icons/content/add'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import addToCartAnimation from '../animations/addToCart'

const style = {
  height: 'auto',
  textAlign: 'center',
  margin: '20px 0',
  cursor: 'pointer'
}

const addButtonStyle = {
  float: 'right',
  minWidth: 'auto',
  width: 'auto',
  paddingTop: 5,
  margin: '0 5px -61px 0px'
}

const onProductAddClick = (e, onAddClick, image) => {
  const el = e.currentTarget.parentElement.parentElement.parentElement.querySelector('.animate-img')
  e.stopPropagation();
  onAddClick();
  addToCartAnimation(el);
}

const Product = ({ onAddClick, onProductClick, name, description, id, image, price }) => (
  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
    <Paper
      style={style}
      zDepth={2}
    >
      <Card onClick={() => onProductClick()}>
        <CardMedia
          overlay={<CardTitle title={name} subtitle={price + ' $'} />}
        >
          <div style={addButtonStyle}>
            <FloatingActionButton onClick={(e) => onProductAddClick(e, onAddClick, image)}>
              <ContentAdd />
            </FloatingActionButton>
          </div>
          <img alt={description} src={image} />
          <img className="animate-img" alt={description} src={image} />
        </CardMedia>
      </Card>
    </Paper>
  </div>
)
Product.propTypes = {
  onAddClick: PropTypes.func.isRequired,
  onProductClick: PropTypes.func.isRequired,
  description: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired
}

export default Product
