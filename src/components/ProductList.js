import React, { PropTypes } from 'react'
import Product from './Product'
import ProductDetails from '../containers/ProductDetails'

const style = {
  margin: '0 10px'
}

const ProductList = ({ products, onAddClick, onProductClick }) => (
  <div>
    <div className="row" style={style}>
      {products.map(product =>
        <Product
          key={product.id}
          {...product}
          onAddClick={() => onAddClick(product)}
          onProductClick={() => onProductClick(product)}
          />
      )}
    </div>
    <ProductDetails/>
  </div>
)

ProductList.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired
  }).isRequired).isRequired,
  onAddClick: PropTypes.func.isRequired,
  onProductClick: PropTypes.func.isRequired,
}

export default ProductList
