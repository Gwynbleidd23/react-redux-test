import { connect } from 'react-redux'
import { setCartState } from '../actions/index'
import TopIconMenu from '../components/TopIconMenu'

const mapStateToProps = (state) => ({})

const mapDispatchToProps = {
  setCartState,
}

const TopIconMenuContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TopIconMenu)

export default TopIconMenuContainer
